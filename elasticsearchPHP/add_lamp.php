<?php
  require 'app/init.php';

  // Datos y su corriespondiente tipo de dato para insertar en cliente:
  //   * id (este se agregará automáticamente)
  //   * user_name string
  //   * mail string
  //   * company string
  //   * token string
  //
  // // Datos y su corriespondiente tipo de dato para insertar en las lámparas
  //   * user_id (recuperado del usuario que se ingresa, este ID se recupera automáticamente también). integer
  //   * lamp_id integer
  //   * lamp_name string
  //   * mac_address string
  //   * latitude float
  //   * longitude float
  //   * creation_date string "format":"yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd",
  //   * updated_date string  "format":"yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd",
  //   * buildings array
  //       * id_building integer
  //       * building_name string
  //       * place string
  //       * floors array
  //           * id_floor integer
  //           * floor_name string

    //datos del PUT
      $index = 'lamps';
      //Users
        $type_user = 'users';
        $id_user = 2;
      //lamps
        $type_lamp = 'lamp';
        $id_lamp = 1;

      //Información usuario
      $user_name = 'User 2';
      $mail = 'user_two@oledcomm.com';
      $company = 'Oledcomm France';
      $token = 'def456fed';

      // Datos array - se recuperará del formulario o del archvio de excel
      $user_id = $id_user;
      $lamp_id = $id_lamp;
      $lamp_name = 'Lamp new';
      $mac_address= 'C2:01:00:00:00:00:00';
      $latitude = 152.011745;
      $longitude = -114.554789;
      $creation_date = '2017-06-26 14:10:30';
      $updated_date = '2017-06-27 14:10:30';
      //buildings
          $id_building = 1;
          $building_name = '';
          $place = '';
          //floors
              $id_floor = 1;
              $floor_name =  '';


      $indexed_lamps = $client->index([
          'index' => $index,
          'type' => $type_lamp,
          'id' => $lamp_id,
          'body' => [
              'user_id' => $user_id,
              'lamp_id' => $lamp_id,
              'lamp_name' => $lamp_name,
              'mac_address'=> $mac_address,
              'latitude' => $latitude,
              'longitude' => $longitude,
              'creation_date' => $creation_date,
              'updated_date' => $updated_date,
              'buildings' =>
              [
                  'id_building' => $id_building,
                  'building_name' => $building_name,
                  'place' => $place,
                  'floors'=>
                  [
                      'id_floor' => $id_floor,
                      'floor_name' => $floor_name
                  ]
              ]
            ]
        ]);


        if($indexed_lamps){
          $resultados = "<h2 style=\"text-align:center\">Lamp added</h2>";
        }


 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <title>LAMPS</title>
</head>
  <body>
    <div class="contenedor">

      <div class="formatimg-div">
        <img class="formatimg" src="elasticsearch_logo.png" alt="">
      </div>
      <h1 class="titulo_index">Oledcomm -lamps  <h2 class="subtitulo_index">Lamps search engine</h2></h1>


      <?php echo $resultados ?>


    </div>
  </body>

</html>
















<!--  -->

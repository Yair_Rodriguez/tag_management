<?php
  require 'app/init.php';

  //datos del PUT
  $index = 'lamps';
  //Users
  $type_user = 'users';
  $response = $client->search([
    'index' => 'lamps',
    'type' => 'users'
  ]);
  $id_user = $response['hits']['total']+1;
  //Lamps
  $response = $client->search([
    'index' => 'lamps',
    'type' => 'lamp'
  ]);
  $id_lamp = $response['hits']['total']+1;

  $lampArr = array();
  for($i=0;$i<=count($_REQUEST['macs']);$i++){
    if($_REQUEST['macs'][$i]!=""){
      $tmpLampArr = array("user_id" => $id_user,
                          "lamp_id" => $id_lamp+$i,
                          "lampName"=> $_REQUEST['lmpid'][$i],
                          "macAddress"=> $_REQUEST['macs'][$i],
                          "lat"=> 0.0,
                          "lng" => 0.0,
                          "creationDate" => date("Y-m-d h:i:s"),
                          "updatedDate" => date("Y-m-d h:i:s"),
                          "idBuilding" => 1,
                          "buildingName" => "",
                          "place_building" => "",
                          "idFloor" => 1,
                          "floorName" => "" );
      array_push($lampArr,$tmpLampArr);
    }
  }

  if (is_array($lampArr) and !empty($lampArr)) {
    $userSet = $client->index([
      'index' => $index,
      'type' => $type_user,
      'id' => $id_user,
      'body' => [
        'user_name' => 'User '.$id_user,
        'mail' => 'user_one@oledcomm.com',
        'company' => 'oledcomm',
        'token' => '_jkl987lkj'
      ]
    ]);

    // Enseguida, en automático al general al nuevo usuario agrega todas las lámpras que le corresponden al usuario
    //  estos datos de las lámparas están en el array anterior y se acoplan según el mapping del archibvo JSON
    foreach($lampArr as $val){
      $lampSet = $client->index([
        'index' => $index,
        'type' => 'lamp',
        'id' => $val['lamp_id'],
        'body' => [
          'user_id' => $val['user_id'],
          'lamp_id' => $val['lamp_id'],
          'lamp_name' => $val['lampName'],
          'mac_address'=> $val['macAddress'],
          'latitude' => $val['lat'],
          'longitude' => $val['lng'],
          'creation_date' => $val['creationDate'],
          'updated_date' => $val['updatedDate'],
          'buildings' => [
            'id_building' => $val['idBuilding'],
            'building_name' => $val['buildingName'],
            'place' => $val['place_building'],
            'floors'=> [
              'id_floor' => $val['idFloor'],
              'floor_name' => $val['floorName']
            ]
          ]
        ]
      ]);
    }

    if($userSet && $lampSet){
      echo "<h5 id='insertResponse' style='text-align:center' class='text-info'>Data successfully added</h5>";
    }
    else{
      echo "<h5 id='insertResponse' style='text-align:center' class='text-error'>Data registration failed</h5>";
    }
  }
  exit;

  // Datos y su corriespondiente tipo de dato para insertar en cliente:
  //   * id (este se agregará automáticamente)
  //   * user_name string
  //   * mail string
  //   * company string
  //   * token string
  //
  // // Datos y su corriespondiente tipo de dato para insertar en las lámparas
  //   * user_id (recuperado del usuario que se ingresa, este ID se recupera automáticamente también). integer
  //   * lamp_id integer
  //   * lamp_name string
  //   * mac_address string
  //   * latitude float
  //   * longitude float
  //   * creation_date string "format":"yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd",
  //   * updated_date string  "format":"yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd",
  //   * buildings array
  //       * id_building integer
  //       * building_name string
  //       * place string
  //       * floors array
  //           * id_floor integer
  //           * floor_name string

?>

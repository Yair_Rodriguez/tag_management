<?php
  require 'app/init.php';

    // Datos y su corriespondiente tipo de dato para insertar en cliente:
    //   * id (este se agregará automáticamente)
    //   * user_name string
    //   * mail string
    //   * company string
    //   * token string
    //
    // // Datos y su corriespondiente tipo de dato para insertar en las lámparas
    //   * user_id (recuperado del usuario que se ingresa, este ID se recupera automáticamente también). integer
    //   * lamp_id integer
    //   * lamp_name string
    //   * mac_address string
    //   * latitude float
    //   * longitude float
    //   * creation_date string "format":"yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd",
    //   * updated_date string  "format":"yyyy-MM-dd'T'HH:mm:ss||yyyy-MM-dd",
    //   * buildings array
    //       * id_building integer
    //       * building_name string
    //       * place string
    //       * floors array
    //           * id_floor integer
    //           * floor_name string

    //datos del PUT
      $index = 'lamps';
      //Users
        $type_user = 'users';
        $id_user = 1;
      //lamps
        $type_lamp = 'lamp';
        $id_lamp = 5;

      //Información usuario
      $user_name = 'User 1';
      $mail = 'user_one@oledcomm.com';
      $company = 'oledcomm';
      $token = 'jkl987lkj';

      // Datos array - Se recupera del archvio de excel
      // Los datos que puse son
     $lamparas = array(
       array("user_id" => $id_user,
            "lamp_id" => $id_lamp,
            "lampName"=>"Lamp 1",
            "macAddress"=>"C0:FD:00:00:00:00:00",
            "lat"=>100.333333,
            "lng" => -50.333333,
            "creationDate" => "2017-06-01 14:10:30",
            "updatedDate" => "2017-06-03 14:10:30",
            "idBuilding" => 1,
            "buildingName" => "",
            "place_building" => "",
            "idFloor" => 1,
            "floorName" => ""),
       array("user_id" => $id_user,
            "lamp_id" => ($id_lamp + 1),
            "lampName"=>"lamp 2",
            "macAddress"=>"C0:FE:00:00:00:00:00",
            "lat"=>100.333333,
            "lng" => -50.333333,
            "creationDate" => "2017-06-02 14:10:30",
            "updatedDate" => "2017-06-03 14:10:30",
            "idBuilding" => 1,
            "buildingName" => "",
            "place_building" => "",
            "idFloor" => 1,
            "floorName" => ""),
       array("user_id" => $id_user,
            "lamp_id" => ($id_lamp + 2),
            "lampName"=>"lamp 3",
            "macAddress"=>"C0:FF:00:00:00:00:00",
            "lat"=>100.333333,
            "lng" => -50.333333,
            "creationDate" => "2017-06-02 14:10:30",
            "updatedDate" => "2017-06-03 14:10:30",
            "idBuilding" => 1,
            "buildingName" => "",
            "place_building" => "",
            "idFloor" => 1,
            "floorName" => ""),
       array("user_id" => $id_user,
            "lamp_id" => ($id_lamp + 3),
            "lampName"=>"lamp 4",
            "macAddress"=>"C1:01:00:00:00:00:00",
            "lat"=>100.333333,
            "lng" => -50.333333,
            "creationDate" => "2017-06-02 14:10:30",
            "updatedDate" => "2017-06-03 14:10:30",
            "idBuilding" => 1,
            "buildingName" => "",
            "place_building" => "",
            "idFloor" => 1,
            "floorName" => "")
     );
     //Así sería como agregaríamos los datos all array con un algún bucle, ya depende de lo que haga Yair
          //  $dato_recuperado_excel;
          //  $lamparas[] = array(
          //               "lampName"=>$dato_recuperado_excel,
          //               "macAddress"=>$dato_recuperado_excel,
          //               "lat"=>$dato_recuperado_excel,
          //               "lng" => $dato_recuperado_excel,
          //               "creationDate" => $dato_recuperado_excel,
          //               "updatedDate" => $dato_recuperado_excel,
          //               "idBuilding" => $dato_recuperado_excel,
          //               "buildingName" => $dato_recuperado_excel,
          //               "place_building" =>$dato_recuperado_excel,
          //               "idFloor" => $dato_recuperado_excel,
          //               "floorName" => $dato_recuperado_excel
          //             );


    if (is_array($lamparas) and !empty($lamparas)) {

      //Información usuario,
      // Esta sección agregaría los datos que se recuperan del formulario
      $params_users = $client->index([
          'index' => $index,
          'type' => $type_user,
          'id' => $id_user,
          'body' => [
            'user_name' => $user_name,
            'mail' => $mail,
            'company' => $company,
            'token' => $token
          ]
        ]);

      // Enseguida, en automático al general al nuevo usuario agrega todas las lámpras que le corresponden al usuario
      //  estos datos de las lámparas están en el array anterior y se acoplan según el mapping del archibvo JSON
    foreach($lamparas as $valor){
      $params_lamps = $client->index([
          'index' => $index,
          'type' => $type_lamp,
          'id' => $valor['lamp_id'],
          'body' => [
              'user_id' => $valor['user_id'],
              'lamp_id' => $valor['lamp_id'],
              'lamp_name' => $valor['lampName'],
              'mac_address'=> $valor['macAddress'],
              'latitude' => $valor['lat'],
              'longitude' => $valor['lng'],
              'creation_date' => $valor['creationDate'],
              'updated_date' => $valor['updatedDate'],
              'buildings' =>
              [
                  'id_building' => $valor['idBuilding'],
                  'building_name' => $valor['buildingName'],
                  'place' => $valor['place_building'],
                  'floors'=>
                  [
                      'id_floor' => $valor['idFloor'],
                      'floor_name' => $valor['floorName']
                  ]
              ]
            ]
        ]);
      }

        if($params_users && $params_lamps){
          $resultados = "<h2 style=\"text-align:center\">User successfully added</h2>";
        }
        else{
          $resultados = "<h2 style=\"text-align:center\">Error al agregar al cliente</h2>";
        }
    }




 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <title>LAMPS</title>
</head>
  <body>
    <div class="contenedor">

      <div class="formatimg-div">
        <img class="formatimg" src="elasticsearch_logo.png" alt="">
      </div>
      <h1 class="titulo_index">Oledcomm -lamps  <h2 class="subtitulo_index">Lamps search engine</h2></h1>


      <?php echo $resultados ?>


    </div>
  </body>

</html>
















<!--  -->

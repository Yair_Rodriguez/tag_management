<?php
  if($_REQUEST['filter'] == "client"){
    echo ResultsQueryClient();
  }
  else if($_REQUEST['filter'] == 'mac') {
    echo ResultsQuery("data_query_mac");
  }
  else if($_REQUEST['filter'] == 'creation') {
    echo ResultsQuery("data_query_creation");
  }
  else if($_REQUEST['filter'] == 'update') {
    echo ResultsQuery("data_query_update");
  }
  else if($_REQUEST['filter'] == 'search') {
    echo ResultsQuery("data_search");
  }

  //Results by client
  function ResultsQueryClient(){
    $reee = data_query_client();
    $html = "";
    if(isset($reee)) {
      if (empty($reee)) {
        $html .= '<div class="result"  style="margin-bottom: 100px;">
                    <h4 class="" > No matches found </h4>
                  </div>';
      } else{
        $html = ' <table class="table">
                    <thead>
                      <tr>
                        <th>User</th>
                        <th>Mail</th>
                        <th>Company</th>
                        <th>Token</th>
                      </tr>
                    </thead>
                    <tbody>';
        foreach ($reee as $r) {
          $html .= '<tr id="usr_'.$r['_id'].'" style="cursor:pointer;" onclick="filterBy(\'client\', \''.$r['_id'].'\')">
                      <td>'.$r['_source']['user_name'].'</td>
                      <td>'.$r['_source']['mail'].'</td>
                      <td>'.$r['_source']['company'].'</td>
                      <td>'.$r['_source']['token'].'</td>
                    </tr>';
        }
        $html .= '  </tbody>
                  </table>';
      }
    }
    return $html;
    exit;
  }

  function data_query_client(){
    $data_array_query = query_client();
    if($data_array_query['hits']['total'] >=1){
      $results = $data_array_query['hits']['hits'];
    } else {
      $results = "";
    }
    return $results;
    exit;
  }

  function query_client() {
    require 'app/init.php';
    $query = $client->search([
      'index' => 'lamps',
      'type' => 'users',
      'body' => [
        'query' => [
          'bool' => [
            'must' => [
              'match_phrase' => [
                'company' => 'oledcomm'
              ]
            ]
          ]
        ]
      ]
    ]);
    return $query;
    exit;
  }

  //Results by MAC, creation date and update date
  function ResultsQuery($name_metod){
    $res = $name_metod();
    $html = "";
    if(isset($res)){
      if (empty($res)) {
        $html .= '<div class="result" style="margin-bottom: 100px;">
                    <h4 class="" > No matches found </h4>
                  </div>';
      } else {
        $html = ' <table class="table">
                    <thead>
                      <tr>
                        <th>Lamp name</th>
                        <th>MAC address</th>
                        <th>User ID</th>
                        <th>Lamp ID</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Latitude</th>
                        <th>Longitude</th>
                      </tr>
                    </thead>
                    <tbody>';
        foreach ($res as $r) {
          $html .= '<tr>
                      <td>'.$r['_source']['lamp_name'].'</td>
                      <td>'.$r['_source']['mac_address'].'</td>
                      <td>'.$r['_source']['user_id'].'</td>
                      <td>'.$r['_source']['lamp_id'].'</td>
                      <td>'.$r['_source']['creation_date'].'</td>
                      <td>'.$r['_source']['updated_date'].'</td>
                      <td>'.$r['_source']['latitude'].'</td>
                      <td>'.$r['_source']['longitude'].'</td>
                    </tr>';
        }
        $html .= '  </tbody>
                  </table>';
      }
    }
    return $html;
    exit;
  }

  //Search by MAC
  function data_query_mac(){
    $data_array_query = query_mac();
    if($data_array_query['hits']['total'] >=1){
      $results = $data_array_query['hits']['hits'];
    } else {
      $results = "";
    }
    return $results;
    exit;
  }

  function query_mac() {
    require 'app/init.php';
    $field = 'mac_address';
    if ($_REQUEST['clientid'] == "-1"){
      $query = $client->search([
        'index' => 'lamps',
        'type' => 'lamp',
        'body' => [
          'query' => [
            'terms' => [
              '_field_names' => [$field]
            ]
          ]
        ]
      ]);
    } else if ($_REQUEST['clientid'] != "-1" && $_REQUEST['tosearch'] == "-"){
      $query = $client->search([
        'index' => 'lamps',
        'type' => 'lamp',
        'body' => [
          'query' => [
            'bool' => [
              'must' => [
                'terms' => [
                  '_field_names' => [$field]
                ]
              ],
              'filter' => [
                'term' => [
                  'user_id' => $_REQUEST['clientid']
                ]
              ]
            ]
          ]
        ]
      ]);
    } else {
      $query = $client->search([
        'index' => 'lamps',
        'type' => 'lamp',
        'body' => [
          'query' => [
            'bool' => [
              'must' => [
                ['match_phrase' => ['mac_address' => $_REQUEST['tosearch']] ]
              ],
              'filter' => [
                'term' => [
                  'user_id' => $_REQUEST['clientid']
                ]
              ]
            ]
          ]
        ]
      ]);
    }
    return $query;
    exit;
  }

  //Search by creation date
  function data_query_creation(){
    $data_array_query = query_creation_date();
    if($data_array_query['hits']['total'] >=1){
      $results = $data_array_query['hits']['hits'];
    } else {
      $results = "";
    }
    return $results;
    exit;
  }

  function query_creation_date() {
    require 'app/init.php';
    $field = 'creation_date';
    if ($_REQUEST['clientid'] == "-1"){
      $query = $client->search([
        'index' => 'lamps',
        'type' => 'lamp',
        'body' => [
          'query' => [
            'terms' => [
              '_field_names' => [$field]
            ]
          ]
        ]
      ]);
    } else {
      $query = $client->search([
        'index' => 'lamps',
        'type' => 'lamp',
        'body' => [
          'query' => [
            'bool' => [
              'must' => [
                'match_phrase' => [
                  'creation_date' => $_REQUEST['tosearch']
                ]
              ],
              'filter' => [
                'term' => [
                  'user_id' => $_REQUEST['clientid']
                ]
              ]
            ]
          ]
        ]
      ]);
    }
    return $query;
    exit;
  }

  //Search by update date
  function data_query_update(){
    $data_array_query = query_update_date();
    if($data_array_query['hits']['total'] >=1){
      $results = $data_array_query['hits']['hits'];
    } else {
      $results = "";
    }
    return $results;
    exit;
  }

  function query_update_date() {
    require 'app/init.php';
    $field = 'updated_date';
    if ($_REQUEST['clientid'] == "-1"){
      $query = $client->search([
        'index' => 'lamps',
        'type' => 'lamp',
        'body' => [
          'query' => [
            'terms' => [
              '_field_names' => [$field]
            ]
          ]
        ]
      ]);
    } else {
      $query = $client->search([
        'index' => 'lamps',
        'type' => 'lamp',
        'body' => [
          'query' => [
            'bool' => [
              'must' => [
                'match_phrase' => [
                  'updated_date' => $_REQUEST['tosearch']
                ]
              ],
              'filter' => [
                'term' => [
                  'user_id' => $_REQUEST['clientid']
                ]
              ]
            ]
          ]
        ]
      ]);
    }
    return $query;
    exit;
  }


  //Search anything
  function data_search(){
    $data_array_query = query_search_any();
    if($data_array_query['hits']['total'] >=1){
      $results = $data_array_query['hits']['hits'];
    } else {
      $results = "";
    }
    return $results;
    exit;
  }

  function query_search_any() {
   require 'app/init.php';
   if ($_REQUEST['clientid'] == "-1"){
     $query = $client->search([
       'index' => 'lamps',
       'type' => 'lamp',
       'body' => [
         'query' => [
           'bool' => [
             'should' => [
               ['match_phrase' => ['lamp_name' => $_REQUEST['tosearch']] ],
               ['match_phrase' => ['mac_address' => $_REQUEST['tosearch']] ],
               ['match_phrase' => ['creation_date' => $_REQUEST['tosearch']] ],
               ['wildcard' => ['mac_address' => $_REQUEST['tosearch'] .'*'] ],
               ['wildcard' => ['lamp_name' => $_REQUEST['tosearch'] .'*'] ],
               ['wildcard' => ['creation_date' => $_REQUEST['tosearch'] .'*'] ]
             ]
           ]
         ]
       ]
     ]);
   } else {
     // query creation_date
     $query_DATE = $client->search([
       'index' => 'lamps',
       'type' => 'lamp',
       'body' => [
         'query' => [
           'bool' => [
             'must' => [
               ['match_phrase' => ['creation_date' => $_REQUEST['tosearch']] ]
             ],
             'filter' => [
               'term' => [
                 'user_id' => $_REQUEST['clientid']
               ]
             ]
           ]
         ]
       ]
     ]);
     $query_lampName = $queryLAMP_NAME['hits']['total'];
     switch (true) {
       case data_query_mac():
         $query = query_mac();
         break;
       case data_query_creation():
         $query = query_creation_date();
         break;
       case $queryLAMP_NAME['hits']['total']:
         $query = $queryLAMP_NAME;
         break;
       default:
         break;
     }
   }
   return $query;
   exit;
 }

?>

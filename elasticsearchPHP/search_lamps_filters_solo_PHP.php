<?php
    //Búsqueda por dirección MAC
        function data_query_mac(){
          $data_array_query = query_mac();
          if($data_array_query['hits']['total'] >=1){
            $results = $data_array_query['hits']['hits'];
          }
          else {
            $results = "";
          }
          return $results;
        }

      function query_mac() {
        require 'app/init.php';
        $query = $client->search([
          'index' => 'lamps',
          'type' => 'lamp',
          'body' =>
          [
            'query' =>
            [
              'bool' =>
              [
                'must' =>
                [
                  'match_phrase' =>
                  [
                    'mac_address' => 'C2:01:00:00:00:00:00' //Este parámetro también se recupera del formulario o archivo de excel
                  ]
                ],
                'filter' => [
                    'term' =>
                    [
                      'user_id' => 2
                    ]
                ]
              ]
            ]
          ]
        ]);
      return $query;
      }

      function data_query_client(){
        $data_array_query = query_client();
        if($data_array_query['hits']['total'] >=1){
          $results = $data_array_query['hits']['hits'];
        }
        else {
          $results = "";
        }
        return $results;
      }

      function query_client() {
        require 'app/init.php';
        $query = $client->search([
          'index' => 'lamps',
          'type' => 'users',
          'body' =>
          [
            'query' =>
            [
              'bool' =>
              [
                'must' =>
                [
                  'match_phrase' =>
                  [
                    'token' => 'def456fed'
                  ]
                ]
              ]
            ]
          ]
        ]);
      return $query;
      }

      //Búsqueda por fecha de creación
      function data_query_creation(){
        $data_array_query = query_fecha_creacion();
        if($data_array_query['hits']['total'] >=1){
          $results = $data_array_query['hits']['hits'];
        }
        else {
          $results = "";
        }
        return $results;
      }

      function query_fecha_creacion() {
        require 'app/init.php';
        $query = $client->search([
          'index' => 'lamps',
          'type' => 'lamp',
          'body' =>
          [
            'query' =>
            [
              'bool' =>
              [
                'must' =>
                [
                  'match_phrase' =>
                  [
                    'creation_date' => '2017-06-01 14:10:30'
                  ]
                ],
                'filter' => [
                    'term' =>
                    [
                      //Al hacer uso de "filter" limites a que busque en sólo un "type" según el parámetro que se le indique, en este caso al usuario con ID 1
                      'user_id' => 1
                    ]
                ]
              ]
            ]
          ]
        ]);
      return $query;
      }

      //Búsqueda por fecha de atualización
      function data_query_update(){
        $data_array_query = query_fecha_actualizacion();
        if($data_array_query['hits']['total'] >=1){
          $results = $data_array_query['hits']['hits'];
        }
        else {
          $results = "";
        }
        return $results;
      }

      function query_fecha_actualizacion() {
        require 'app/init.php';
        $query = $client->search([
          'index' => 'lamps',
          'type' => 'lamp',
          'body' =>
          [
            'query' =>
            [
              'bool' =>
              [
                'must' =>
                [
                  'match_phrase' =>
                  [
                    'updated_date' => '2017-06-03 14:10:30'
                  ]
                ],
                'filter' => [
                    'term' =>
                    [
                      'user_id' => 2 //Al hacer uso de "filter" limites a que busque en sólo un "type" según el parámetro que se le indique, en este caso usuario con ID 2
                    ]
                ]
              ]
            ]
          ]
        ]);
      return $query;
      }

 ?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css" type="text/css">
  <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> -->
  <title>LAMPS</title>
</head>
  <body>
    <div class="formatimg-div">
      <img class="formatimg" src="elasticsearch_logo.png" alt="">
    </div>
    <h1 class="titulo_index">Oledcomm -lamps  <h2 class="subtitulo_index">Lamps search engine</h2></h1>


        <div class="result">
          <h3 class="">Filtros búsqueda</h3>
          <form class="estiloForm" action="search_lamps_filters_solo_PHP.php" method="post">
            <!-- <button class="style_button" type="button" name="button">clic</button> -->
            <input type="submit" name="queryClient" value="query Client" />
            <input type="submit" name="queryMAC" value="queryMAC" />
            <input type="submit" name="queryCreation" value="queryCreation" />
            <input type="submit" name="queryUpdate" value="queryUpdate" />

          </form>
        </div>

         <?php
         if($_POST){
           if (isset($_POST['queryClient'])) {
            ResultsQueryClient();
           }
           if (isset($_POST['queryMAC'])) {
            ResultsQuery("data_query_mac");
           }
           if (isset($_POST['queryCreation'])) {
            ResultsQuery("data_query_creation");
           }
           if (isset($_POST['queryUpdate'])) {
            ResultsQuery("data_query_update");
           }
        }


        // Resultados búsqueda por cliente
         function ResultsQueryClient(){
           $reee = data_query_client();
           if(isset($reee))
           {?>
             <h2 class="tit">Results</h2>
             <?php
             if (empty($reee)) {
               ?>
               <div class="result"  style="margin-bottom: 100px;">
                 <h4 class="titulo_articulo_encontrado" > Sin resultados </h4>
               </div>
               <?php
             }
             else{
               foreach ($reee as $r)
               {?>
                 <div class="result" style="margin-bottom: 100px;">
                   <h4 class="titulo_articulo_encontrado" ><?php echo "<b>User:</b> " . $r['_source']['user_name']; ?></h4>
                   <div class="result-keywords">
                     <?php echo "<b>Mail:</b> " . $r['_source']['mail'];  ?> <br>
                     <?php echo "<b>Company:</b> " . $r['_source']['company'];  ?> <br>
                     <?php echo "<b>Token:</b> " . $r['_source']['token'];  ?> <br>
                   </div>
                 </div>
                 <?php
               }
             }
           }
         }

             // Resultados búsqueda por dirección MAC, fecha de creación y actualización
          function ResultsQuery($name_metod){
            $res = $name_metod();
            if(isset($res))
            {?>
              <h2 class="tit">Results</h2>
              <?php
              if (empty($res)) {
                ?>
                <div class="result" style="margin-bottom: 100px;">
                  <h4 class="titulo_articulo_encontrado" > Sin resultados </h4>
                </div>
                <?php
              }
              else{
                foreach ($res as $r) {
                  ?>
                  <div class="result" style="margin-bottom: 100px;">
                    <h4 class="titulo_articulo_encontrado" ><?php echo "<b>Lamp name:</b> " . $r['_source']['lamp_name']; ?></h4>
                    <div class="result-keywords">
                      <?php echo "<b>MAC address: </b> " . $r['_source']['mac_address'];  ?> <br>
                      <?php echo "<b>User ID: </b> " . $r['_source']['user_id'] . ", <b>Lamp ID:</b> " . $r['_source']['lamp_id'];  ?> <br>
                      <?php echo "<b>Creation date: </b> " . $r['_source']['creation_date'];  ?> <br>
                      <?php echo "<b>Updated date: </b> " . $r['_source']['updated_date'];  ?> <br>
                      <?php echo "<b>Latitude: </b> " . $r['_source']['latitude'] . ", <b>Longitude:</b> " . $r['_source']['longitude'];  ?>
                    </div>
                  </div>
                  <?php

                }
              }
            }
          }



         ?>  <!-- fin instrucciones PHP -->


  </body>

</html>

















<!--  -->

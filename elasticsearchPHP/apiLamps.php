
<?php
/**
 * Plugin interface. Parsing JSON formats from Elasticsearch to the database of the LiFi Cloud
 * @author Brenda Rangel Cervantes
 *
 */

  if ($_REQUEST[param] == "getLamps"){
  	echo ResultsQuery();
    generateSQLFile();
  } else if ($_REQUEST[param] == 'exportToCloud'){
  	insertLamps();
  }

  function ResultsQuery(){
  	$queryArray = query_lamp();
    sort($queryArray);
    if($queryArray == 'nothing'){
        echo "<h5 id='insertResponse' style='text-align:center' class='text-info'>Nothing to do.</h5>";
    }else{
      $html = "";
    	if (isset($queryArray)){
    		if (empty($queryArray)){
    			$html.= '<div class="result"  style="margin-bottom: 100px;">
                      <h4 class="" > No matches found </h4>
                    </div>';
    		}else{
    			$html = ' <table class="table">
                      <thead>
                        <tr>
                          <th>Clave l�mpara</th>
                          <th>Mac address</th>
                          <th>User</th>
                        </tr>
                      </thead>
                      <tbody>';
    			foreach($queryArray as $r){
    				$html.= '<tr style="cursor:pointer;">
                        <td>' . $r['_source']['lamp_id'] . '</td>
                        <td>' . $r['_source']['mac_address'] . '</td>
                        <td>' . $r['_source']['user_id'] . '</td>
                      </tr>';
    			}
    			  $html.= '</tbody>
                    </table>';
    		}
      }
    }
  	return $html;
  	exit;
  }

  function query_lamp(){
  	require 'app/init.php';
  	$size = 10000;
  	$query = $client->search([
      'index' => 'lamps',
      'type' => 'lamp',
      'size' => $size,
  	]);
    if ($query['hits']['total'] >= 1){
  		$results = $query['hits']['hits'];
  	} else{
  		$results = "nothing";
  	}
  	return $results;
  	exit;
  }

  function insertLamps(){
  	$servername = "localhost";
  	$username = "root";
  	$password = "";
  	//$dbname = "olcserver_tagsystem";
  	$dbname = "olcserver_development";

  	$conn = new mysqli($servername, $username, $password, $dbname);
  	// Check connection
  	if ($conn->connect_error){
  		die("Connection failed: " . $conn->connect_error);
  	}
    $queryArray = query_lamp();
    if($queryArray == 'nothing'){
        echo "<h5 id='insertResponse' style='text-align:center' class='text-info'>Nothing to do.</h5>";
    }else{
      foreach($queryArray as $row){
    		$lamp_id = $row['_source']['lamp_id'];
    		$lamp_name = $row['_source']['lamp_name'];
    		$mac_address = $row['_source']['mac_address'];
    		$user_id = $row['_source']['user_id'];
    		$creation_date = $row['_source']['creation_date'];
    		$updated_date = $row['_source']['updated_date'];
    		$latitude = $row['_source']['latitude'];
    		$longitude = $row['_source']['longitude'];
        $sql = "INSERT INTO lamps (id, name, mac_address, user_id, created_at, updated_at, latitude, longitude)
                VALUES('$lamp_name', '$lamp_name', '$mac_address','$user_id', '$creation_date', '$updated_date', '$latitude', '$longitude')";
        $conn->query($sql);
        $error = $conn->error;
      }
      if(empty($error)){
        echo "<h5 id='insertResponse' style='text-align:center' class='text-info'>All lamps were exported.</h5>";
      } else {
        echo "<h5 id='insertResponse' style='text-align:center' class='text-error'>All the lamps present in the table were already exported. </h5>";
      }
    }
    $conn->close();
    exit;
  }

  function generateSQLFile(){
    $queryArray = query_lamp();
    if($queryArray == 'nothing'){
        echo "<h5 id='insertResponse' style='text-align:center' class='text-info'>Nothing to do.</h5>";
    }
    else{
      foreach($queryArray as $row){
    		$lamp_id = $row['_source']['lamp_id'];
    		$lamp_name = $row['_source']['lamp_name'];
    		$mac_address = $row['_source']['mac_address'];
    		$user_id = $row['_source']['user_id'];
    		$creation_date = $row['_source']['creation_date'];
    		$updated_date = $row['_source']['updated_date'];
    		$latitude = $row['_source']['latitude'];
    		$longitude = $row['_source']['longitude'];
        $sql = "INSERT INTO lamps (id, name, mac_address, user_id, created_at, updated_at, latitude, longitude)
                VALUES('$lamp_name', '$lamp_name', '$mac_address','$user_id', '$creation_date', '$updated_date', '$latitude', '$longitude');";
        $content = $content . $sql;
        sort($content);
      }
      if(empty($content)){
        echo "<h5 id='insertResponse' style='text-align:center' class='text-info'>bad.</h5>";
      }else {
        $file_name = 'lamps.sql';
        $dir = "../";
        $file = file_put_contents($dir.$file_name, $content);
      }
    }
    return $content;
  }


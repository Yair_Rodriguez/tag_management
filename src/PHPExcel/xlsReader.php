<?php
include 'excel_reader.php';     // include the class
if(isset($_FILES['file']['name'])){
  move_uploaded_file($_FILES["file"]["tmp_name"],"../../uploads/".$_FILES['file']['name']);
}else{
  exit;
}

// creates an object instance of the class, and read the excel file data
$excel = new PhpExcelReader;
$excel->read("../../uploads/".$_FILES['file']['name']);
// this function creates and returns a HTML table with excel rows and columns data
// Parameter - array with excel worksheet data
function sheetData($sheet) {
  $re = '<table>';     // starts html table
  $x = 1;
  while($x <= $sheet['numRows']) {
    $re .= "<tr>\n";
    $y = 1;
    while($y <= $sheet['numCols']) {
      $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
      $re .= " <td id='dta_$x$y' onclick='setinptForm(\"dta_$x$y\")'>".utf8_encode($cell)."</td>\n";
      $y++;
    }
    $re .= "</tr>\n";
    $x++;
  }
  return $re .'</table>';     // ends and returns the html table
}

$nr_sheets = count($excel->sheets);       // gets the number of sheets
$excel_data = '';              // to store the the html tables with data of each sheet

// traverses the number of sheets and sets html table with each sheet data in $excel_data
for($i=0; $i<$nr_sheets; $i++) {
  $excel_data .= '<h4>Sheet '. ($i + 1) .' (<em>'. $excel->boundsheets[$i]['name'] .'</em>)</h4>'. utf8_encode(sheetData($excel->sheets[$i])) .'<br/>';
}
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Hello curious debugger!</title>
<style type="text/css">
table {
 border-collapse: collapse;
 overflow-x: scroll;
 overflow-y: auto;
 max-height: 20%;
}
td {
 border: 1px solid #AAA;
 padding: 0.5em 0.5em;
 font-size: 14px;
}
</style>
</head>
<body>

<?php
// displays tables with excel file data
echo $excel_data;
?>

</body>
</html>

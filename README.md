# README #

### Introduction ###
This repository contains the code base for the Oledcomm Tag management system which functions as a lamp search system. Lamps are stored in the Elasticsearch Data Manager, which can then be accessed through the tag management system from any web client.

It includes:



### Lamp management ###
* Importing lamps data from .xls files.
* Conserve lamps data in the Elasticsearch database.
* Quick and real time search of the lamps by tags or dynamic searches.



### Setup Environment ###
The environment choossen for this web system is PHP 7.0.18 along with Elasticsearch 5.4.3; versions at hand when developed. For database management the same plugin included was used.

JRE and JDK previously installed are required for [ElastiscSearch 5.4.3] to work, then please refer to the following guide. 
(https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html)<-- link for installer with instructions and tutorials

* Configuration
* Dependencies
* How to run tests
* Deployment instructions
-->

Once the Elasticsearch service is enabled, you will need to configure the elasticsearch.yml file on the lines:

* cluster.name:
* node.name:

Then enable the demon and it will be ready to use.



### Site ###
When Elasticsearch and the PHP server are propperly set up, in order to use the site and project just copy the content of the html folder into your /htdocs or /www folder.
AND create a forlder inside called "uploads" with permission to be read and write.



### Kibana ###
You can use Kibana which is a window into the Elastic Stack, it enables visual exploration and real-time analysis of your data in Elasticsearch, as a dev tool.
To install [Kibana 5.4.3] follow the next guide:
(https://www.elastic.co/guide/en/kibana/current/deb.html)
To configure uncomment the following:
* server.port:
* server.host:
* server.name:
* elasticsearch.name:
* kibana.index:
Then enable the demon and ready, try using (http://localhost:5601)

### Revision ###

* [Yair Fabian](mailto:yair.fabian@oledcomm.com) 04/07/17
* [Cesar Perez](mailto:cesar.perez@oledcomm.com) 04/07/17
* [Brenda Rangel](mailto:brenda.rangel@oledcomm.com) 04/07/17
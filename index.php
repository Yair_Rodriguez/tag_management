<?php
/*  Oledcomm 2017
*/
?>


<html>
  <head>
    <title>Tag management</title>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="src/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="src/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="src/css/extraStyles.css" />
    <link rel="stylesheet" type="text/css" href="src/fonts/styles/fontello.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto|Work+Sans" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="src/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="">
      <div class="row-fluid">
        <div class="col-lg-2 col-m-4 col-sm-4 sidebar">
          <form class="estiloForm" action="index.php" method="get" autocomplete="off">
            <div class="input-group col-lg-12 col-m-12 col-sm-12">
              <span class="inline icon-search infield_icon col-lg-1 col-m-1 col-sm-1" id="basic-addon1" onclick="togglePane()"></span>
              <input type="text" class="inline infield_search col-lg-11 col-m-11 col-sm-11" name="q" id="q" onkeypress="elasticquery('search')" placeholder=" Search" aria-describedby="basic-addon1">
            </div>
            <div class="col-lg-12 col-m-12 col-sm-12 side_section" id="activefilters">
              <input type="text" class="hidden" value="-1" id="usrid"/>
              <div id="acFilterList">
              </div>
            </div>
            <div class="col-lg-12 col-m-12 col-sm-12 side_section" id="filters">
              <h4 onclick="toggleFilters()">
                <span class="inline icon-list-add"></span>
                Search all by
                <span id="lisToggle" class="inline icon-right-open pull-right"></span>
              </h4>
              <div class"col-lg-12 col-m-12 col-sm-12" id="filterList">
                <div class="chkOption" onclick="elasticquery('client')">
<!--                  <input id="checkbox-1" class="checkbox-custom hidden" name="checkbox-1" type="checkbox">-->
                  <label class="checkbox-custom-label">Client</label>
                </div>
                <div class="chkOption" onclick="elasticquery('mac')">
<!--                  <input id="checkbox-2" class="checkbox-custom hidden" name="checkbox-2" type="checkbox">-->
                  <label class="checkbox-custom-label">MAC address</label>
                </div>
                <div class="chkOption" onclick="elasticquery('creation')">
<!--                  <input id="checkbox-3" class="checkbox-custom hidden" name="checkbox-3" type="checkbox">-->
                  <label class="checkbox-custom-label">Creation date</label>
                </div>
                <div class="chkOption" onclick="elasticquery('update')">
<!--                  <input id="checkbox-4" class="checkbox-custom hidden" name="checkbox-4" type="checkbox">-->
                  <label class="checkbox-custom-label">Update date</label>
                </div>
              </div>
            </div>
            <input id="submitForm" type="submit" value="Search" class="hidden">
          </form>
          <div class="col-lg-12 col-m-12 col-sm-12 side_section">
            <h4 onclick="loadsection('upload')">
              <span class="inline icon-plus"></span>
              Add lamps
            </h4>
          </div>
        </div>
        <div class="col-lg-10 col-m-8 col-sm-8 content">
          <img class="img-background" src="/src/bkgrnd.jpg">
          <div class="side-panel">
            <div class="pull-left inline">
              <h1 id="responseTitle">Why did you click that magnifier glass?... </br>that's for debugging purposes.</br></br> ¬¬</h1>
            </div>
            <div class="pull-right inline" id="responseComp"></div>
            <div id="response" class="col-lg-12 col-m-12 col-sm-12">
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>

<script>
  $( document ).ready(function() {
    if($(window).height() > $(window).width()){
      $(".img-background").css({"width":"auto", "height":"100%"});
    }else{
      $(".img-background").css({"width":"100%", "height":"auto"});
    }
  });

  toggleFilters();
  function toggleFilters(){
    $(".chkOption").each(function(){
      $(this).fadeToggle(200);
    });
    $("#lisToggle").toggleClass("rot90fwd");
  }

  function togglePane(){
    var wpercent="50%";
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
      wpercent="100%";
    }
    if($(".side-panel").css("opacity") == 0) {
      $(".side-panel").animate({
        opacity:1,
        width:wpercent
      }, 600);
    }else{
      $(".side-panel").animate({
        opacity:0,
        width:0
      }, 600);
    }
  }

  function loadsection(section){
    $("#insertResponse").remove();
    $("#response, #responseTitle, #responseComp").hide();
    if(section=='upload'){
      $("#responseTitle").html('Add Lamps');
      $("#responseTitle").addClass('icon-plus');
      $("#responseComp").html(' <form enctype="multipart/form-data" action="/src/PHPExcel/xlsReader.php" method="post" id="xlsForm">\
                                  <input type="file" name="file" id="file" class="inputfile" onchange="uploadXls()"/>\
                                  <label for="file" class="icon-upload"> Add from .xls</label>\
                                  <input id="submitXls" class="hidden" type="submit" value="Submit" />\
                                  <div class="pull-right inline btn btn-success icon-floppy" id="saveLamps" onclick="saveLamps()">Save</div>\
                                  <div class="pull-right inline btn btn-info icon-plus-circled" id="addLamps" onclick="addLamp()">Add Lamp</div>\
                                </form>');
      $("#response").html(' <div id="elasticForm" class="elasticform scrollable">\
                              <div id="lf_0" class="lampform col-lg-12 col-m-12 col-sm-12">\
                                <span class="inline pull-left">\
                                  <i class="btn btn-sm icon-cancel inptDel" onclick=""></i>\
                                </span>\
                                <div class="inline pull-left">\
                                  <input id="mac_0" class="inptform inptmac" placeholder="MAC address"/>\
                                  <input id="id_0" class="inptform inptid" placeholder="Lamp id"/>\
                                </div>\
                              </div>\
                            </div>');
        $(".scrollable").css("max-height", "90%");
    }
    if($(".side-panel").css("opacity") == 0) {
      togglePane();
    }
    $("#response, #responseTitle").fadeIn('fast');
    if(section=='upload'){
      $("#responseComp").fadeIn('fast');
    }

  }

  var onfield=0;
  function uploadXls(){
    $("#response").fadeOut('fast');
    $.ajax({
      url: "src/PHPExcel/xlsReader.php",
      type: 'POST',
      data: new FormData( $("#xlsForm")[0] ),
      mimeType: "multipart/form-data",
      async: false,
      cache: false,
      contentType: false,
      processData: false,
      success: function (result) {
          $("#response").hide();
          $("#response").html('<div class="scrollable">'+result+'</div>');
          $("#response").append(' <h4 class="text-center text-warning">To add lamps <b>first click on the mac and after its id on the table</b>, the values will be added on the fields below, <small class="text-warning"></br>a new group field will be created after selection of each id.</small></h4>');
          $("#response").append(' <div id="elasticForm" class="elasticform scrollable">\
                                    <div id="lf_0" class="lampform col-lg-12 col-m-12 col-sm-12">\
                                      <span class="inline pull-left">\
                                        <i class="btn btn-sm icon-cancel inptDel" onclick=""></i>\
                                      </span>\
                                      <div class="inline pull-left">\
                                        <input id="mac_0" class="inptform inptmac" placeholder="MAC address"/>\
                                        <input id="id_0" class="inptform inptid" placeholder="Lamp id"/>\
                                      </div>\
                                    </div>\
                                  </div>');
          $(".scrollable").css("max-height", "38%");
          $("#response").fadeIn('fast');
          $("#mac_0").focus();
          onfield=0;
      }
    });
  }

  function setinptForm(element){
    var content=$("#"+element).html();
    content=((content=="")? "-":content);
    switch(onfield){
      case 0: $('.inptmac').each(function(){
                if($(this).val()==""){
                  $(this).val(content);
                  onfield=1;
                  return false;
                }
              });
              break;
      case 1: $('.inptid').each(function(){
                if($(this).val()==""){
                  $(this).val(content);
                  onfield=0;
                  addLamp();
                  return false;
                }
              });
              break;
    }
  }

  var lampfields=0;
  function addLamp(){
    lampfields=lampfields+1;
    $("#elasticForm").append('<div id="lf_'+lampfields+'" class="lampform col-lg-12 col-m-12 col-sm-12" style="display:none;">\
                                <span class="inline pull-left">\
                                  <i class="btn btn-danger btn-sm icon-cancel inptDel" onclick="$(\'#lf_'+lampfields+'\').remove()"></i>\
                                </span>\
                                <div class="inline pull-left">\
                                  <input id="mac_'+lampfields+'" class="inptform inptmac" placeholder="MAC address"/>\
                                  <input id="id_'+lampfields+'" class="inptform inptid" placeholder="Lamp id"/>\
                                </div>\
                              </div>');
    $('#lf_'+lampfields).fadeIn();
  }

  function saveLamps(){
    var i=0;
    var macs=new Array();
    var lmpid=new Array();
    $(".lampform").each(function(){
      macs[i] = $('#mac_'+i).val();
      lmpid[i] = $('#id_'+i).val();
      i++;
    });
    $.ajax({
      url: "elasticsearchPHP/addElasticsearch.php",
      type: 'POST',
      data: {"macs":macs,
             "lmpid":lmpid},
      async: false,
      success: function (result) {
        $("#insertResponse").remove();
        $("#response").append(result);
      }
    });
  }

  function elasticquery(filter){
    $.ajax({
      url: "elasticsearchPHP/searchElasticsearch.php",
      type: 'POST',
      data: { 'filter': filter,
              'clientid': $("#usrid").val(),
              'tosearch': $('#q').val()+"-"},
      success: function(result){
        $("#responseTitle").html('Search all by '+filter.toLowerCase());
        if(filter=="search"){
          $("#responseTitle").html('General search');
        }
        if(filter=="client"){
          $("#responseTitle").append('<h5 class="text-warning" style="margin-left:50px"></br>\
                                        Users can also be used as filters, <b>click on one to search their lamps.</b>\
                                      </h5>');
        }
        $("#responseTitle").addClass('icon-search');
        $("#response").html(' <div id="searchRes" class="scrollable">'+result+'</div>');
        $(".scrollable").css("max-height", "90%");
        loadsection('nosection');
      }
    });
  }

  function filterBy(param, element){
    $("#usrid").val(element);
    $("#acFilterList").html("");
    if(element > 0){
      $("#acFilterList").append('<i id="'+param+'_'+element+'" class="paramList icon-cancel" onclick="filterBy(\''+param+'_'+element+'\', -1)">'+$("#usr_"+element).find("td:first").html()+'</br></i>');
      $("#activefilters").fadeIn();
      param=((param=='client')? 'mac':param);
      elasticquery(param);
    } else {
      $("#activefilters").fadeOut();
      $('#'+param).remove();
    }
  }
</script>
